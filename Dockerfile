FROM python:3.10.0-slim
RUN mkdir test_task
WORKDIR /test_task

COPY  poetry.lock pyproject.toml ./

RUN python -m pip install --no-cache-dir poetry==1.8.3 \ 
    && poetry config virtualenvs.create false \
    && poetry install --no-dev

COPY ./test_task ./test_task

CMD ["python", "test_task/main.py"]