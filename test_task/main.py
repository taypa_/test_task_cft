import contextlib
from fastapi import FastAPI, status, Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi.responses import RedirectResponse
from jose import JWTError
import sys

sys.path.append("./test_task")

from config.database import db_config, shemas
from config import crypt

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

@contextlib.asynccontextmanager
async def lifespan(app: FastAPI):
    is_empty = db_config.create_tables()
    if is_empty:
        db_config.insert_data_tables()
    print("БД готова")
    yield
    print()

app = FastAPI(lifespan=lifespan)

#Проверка наличия логина в БД
def check_user(login: str, password: str):
    session = db_config.db_connect()
    user = session.query(shemas.Login.id, shemas.Login.password).filter(shemas.Login.login == login).all()
    db_config.db_close(session)
    if len(user) != 1:
        return None
    if not crypt.verify_password(password, user[0][1]):
        return None
    return user[0]

#Получение id пользователя по логину
def get_user_id(login):
    session = db_config.db_connect()
    user_id = session.query(shemas.Login.id).filter(shemas.Login.login == login).all()[0][0]
    db_config.db_close(session)
    return user_id

#Получение информации о ЗП и дате повышения
def get_info_user(user_id: int):
    session = db_config.db_connect()
    salary = session.query(shemas.Salary.salary).filter(shemas.Salary.id == user_id).all()[0][0]
    raising = session.query(shemas.Raising.date_raising).filter(shemas.Raising.id == user_id).all()[0][0]
    db_config.db_close(session)
    return {"salary": salary,
            "date_raising": raising}

def write_token(user_id,token, token_type,time_start, time_end):
    try:
        session = db_config.db_connect()
        token_db = shemas.Token()
        token_db.user_id = user_id
        token_db.token = token
        token_db.token_type = token_type
        token_db.time_start = time_start
        token_db.time_end = time_end
        session.add(token_db)
        session.commit()
    except Exception:
        raise Exception("Ошибка при вставке в  базу  данных")

@app.get("/")
def redirect():
    return RedirectResponse("/docs")

@app.post("/login")
def auth(form : OAuth2PasswordRequestForm = Depends()):
    if check_user(form.username, form.password) is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Неверные имя пользователя или пароль",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token,  time_start, time_end = crypt.create_token(data={'sub': form.username})
    user_id = get_user_id(form.username)
    write_token(user_id,  access_token, "bearer", time_start, time_end)
    return {"access_token":access_token, 
            "token_type":"bearer"}

@app.get("/info/")
def info_user(token: str= Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Не удалось подтвердить учетные данные",
        headers={"WWW-Authenticate": "Bearer"})
    
    decode_token = crypt.verify_jwt_token(token)
    if decode_token is JWTError:
        raise credentials_exception
    
    user_name = decode_token.get("sub")
    if user_name is None:
        raise credentials_exception

    user_id = get_user_id(user_name)
    if user_id is None:
        raise credentials_exception

    user_info = get_info_user(user_id)
    if user_info is None:
        raise credentials_exception

    return user_info

if __name__=="__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)