from jose import JWTError, jwt
from passlib.context import CryptContext
from datetime import datetime, timedelta, timezone

ACCESS_TOKEN_EXPIRE_MINUTES = 1
ALGORITHM = "HS256"
SECRET_KEY = "SUPERSECRETKEYFORCFT"

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def verify_password(password: str, hash_pass: str):
    return pwd_context.verify(password, hash_pass)

def create_token(data: dict):
    encode_data = data.copy()
    time_now = datetime.now(timezone.utc)
    expire = time_now + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    encode_data.update({"exp": expire})
    encode_jwt = jwt.encode(encode_data,
                            algorithm=ALGORITHM,
                            key=SECRET_KEY)
    return encode_jwt, time_now, expire

def verify_jwt_token(token):
    try:
        decoded_data = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        return decoded_data
    except JWTError:
        return JWTError