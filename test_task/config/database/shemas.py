from sqlalchemy import Integer, String, ForeignKey, Column, null
from sqlalchemy.orm import DeclarativeBase


class Model(DeclarativeBase):
    pass

class Login(Model):
    __tablename__ = 'login'

    id = Column(Integer, primary_key=True, comment='ID пользователя')
    login = Column(String(100), nullable=False, unique=True, comment='Логин пользователя')
    password = Column(String(256), nullable=False, comment='Захешированный пароль пользователя')

class Token(Model):
    __tablename__='token'
    id = Column(Integer, primary_key=True,  autoincrement=True)
    user_id = Column(Integer, ForeignKey('login.id'), nullable=False)
    token = Column(String(255), nullable=False)
    token_type = Column(String(255), nullable=False)
    time_start = Column(Integer, nullable=False)
    time_end = Column(Integer, nullable=False)

class Salary(Model):
    __tablename__ = 'salary'
    id = Column(Integer, ForeignKey('login.id'), primary_key=True, comment='ID пользователя' )
    salary = Column(Integer, nullable=False,  comment='Зарплата пользователя')

class Raising(Model):
    __tablename__ = 'raising'
    id = Column(Integer, ForeignKey('login.id'), primary_key=True, comment='ID пользователя')
    date_raising = Column(String(10), nullable=False, comment='Дата повышения пользователя')
