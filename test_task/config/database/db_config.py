from sqlalchemy import create_engine, insert
import sqlalchemy
from sqlalchemy.orm  import sessionmaker
import config.database.shemas as shemas

DATABASE_URL = "sqlite:///./test_task/config/database/db.sqlite"
engine = create_engine(DATABASE_URL)



def db_connect():
    Session = sessionmaker(bind=engine)
    session = Session()
    return session

def db_close(session):
    session.close()

def create_tables():
    table_names = sqlalchemy.inspect(engine).get_table_names()
    if table_names == []:
        shemas.Model.metadata.create_all(engine)
        return True
    return False

def insert_data_tables():
    login_data = [{
        "id": 1,
        "login": "ivanov",
        "password": "$2b$12$s1FzF4UrySCvr3feLDeepOj4ElSjTQ2mcpXW8mTfLLTmG13B1QhQm"
    },{
        "id": 2,
        "login": "danilov",
        "password": "$2b$12$s1FzF4UrySCvr3feLDeepOj4ElSjTQ2mcpXW8mTfLLTmG13B1QhQm"
    },{
        "id": 3,
        "login": "sergeev",
        "password": "$2b$12$s1FzF4UrySCvr3feLDeepOj4ElSjTQ2mcpXW8mTfLLTmG13B1QhQm"
    }]
    salary_data = [{
        "id": 1,
        "salary": 10000
    },{
        "id": 2,
        "salary": 30000
    },{
        "id": 3,
        "salary": 50000
    }]
    raising_data = [{
        "id": 1,
        "date_raising": "01.01.2025"
    },{
        "id": 2,
        "date_raising": "20.06.2025"
    },{
        "id": 3,
        "date_raising": "01.01.2026"
    }]

    conn = engine.connect()
    conn.execute(insert(shemas.Login), login_data)
    conn.commit()
    conn.execute(insert(shemas.Salary), salary_data)
    conn.commit()
    conn.execute(insert(shemas.Raising), raising_data)
    conn.commit()