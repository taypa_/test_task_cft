import time
from fastapi.testclient import TestClient
from test_task.main import app

client = TestClient(app)
tokens = []
usernames = ["ivanov", "danilov", "sergeev"]

#Проверка неправильного пароля и логина
def test_invalid_auth():
    response = client.post("/login", data={"username": "ivanov",
                                    "password": "123", "grant_type":"",
                                    "scope":"", "client_id":"", "client_secret":""},
                                    headers={"content-type": "application/x-www-form-urlencoded"})
    assert response.status_code == 401
    assert "Неверные имя пользователя или пароль" in response.json().get("detail")

    response = client.post("/login", data={"username": "ivano",
                                    "password": "12345", "grant_type":"",
                                    "scope":"", "client_id":"", "client_secret":""},
                                    headers={"content-type": "application/x-www-form-urlencoded"})
    assert response.status_code == 401
    assert "Неверные имя пользователя или пароль" in response.json().get("detail")

#Проверка правильных пользователей
def test_valid_user():
    response = client.post("/login",data={"username": "ivanov",
                                    "password": "12345", "grant_type":"",
                                    "scope":"", "client_id":"", "client_secret":""},
                                    headers={"content-type": "application/x-www-form-urlencoded"})
    assert response.status_code == 200
    assert "access_token" in response.json()
    assert response.json().get("token_type") == "bearer"
    tokens.append(response.json().get("access_token"))

    response = client.post("/login",data={"username": "danilov",
                                    "password": "12345", "grant_type":"",
                                    "scope":"", "client_id":"", "client_secret":""},
                                    headers={"content-type": "application/x-www-form-urlencoded"})
    assert response.status_code == 200
    assert "access_token" in response.json()
    assert response.json().get("token_type") == "bearer"
    tokens.append(response.json().get("access_token"))

    response = client.post("/login",data={"username": "sergeev",
                                    "password": "12345", "grant_type":"",
                                    "scope":"", "client_id":"", "client_secret":""},
                                    headers={"content-type": "application/x-www-form-urlencoded"})
    assert response.status_code == 200
    assert "access_token" in response.json()
    assert response.json().get("token_type") == "bearer"
    tokens.append(response.json().get("access_token"))

#Проверка доступа к /info/ с валидным токеном
def test_valid_token():
    response = client.get("/info/",  headers={
        "accept" : "application/json",
        "Authorization":  f"Bearer {tokens[0]}"
    })
    assert response.status_code == 200
    assert response.json().get("salary") == 10000
    assert response.json().get("date_raising") == "01.01.2025"

    response = client.get("/info/",  headers={
        "accept" : "application/json",
        "Authorization":  f"Bearer {tokens[1]}"
    })
    assert response.status_code == 200
    assert response.json().get("salary") == 30000
    assert response.json().get("date_raising") == "20.06.2025"

    response = client.get("/info/",  headers={
        "accept" : "application/json",
        "Authorization":  f"Bearer {tokens[2]}"
    })
    assert response.status_code == 200
    assert response.json().get("salary") == 50000
    assert response.json().get("date_raising") == "01.01.2026"

#Проверка доступа к /info/ с невалидным токеном
def test_invalid_token():
    invalid_token = tokens[0][:-1] + chr(ord(tokens[0][-1])+6)
    response = client.get("/info/",  headers={
        "accept" : "application/json",
        "Authorization":  f"Bearer {invalid_token}"
    })
    assert response.status_code == 401
    assert "Не удалось подтвердить учетные данные" in response.json().get("detail")

#Проверка доступа к /info/ с истекшим токеном
def test_expired_token():
    time.sleep(61)
    response = client.get("/info/",  headers={
        "accept" : "application/json",
        "Authorization":  f"Bearer {tokens[1]}"
    })
    assert response.status_code == 401
    assert "Не удалось подтвердить учетные данные" in response.json().get("detail")
